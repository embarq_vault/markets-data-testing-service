const functions = require('firebase-functions');
const { app } = require('./src/index');

module.exports.marketAnalyticsTestingServer = functions.region('europe-west1').https.onRequest(app);