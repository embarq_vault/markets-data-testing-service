// @ts-check

const TESTING_SERVER_PORT = 2371;
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const logger = require('express-simple-logger');

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(logger());
app.use(compression());

const {
  countProducts,
  countProductChanges,
  countProductsGrandTotal,
  getMostViableGameChanger,
  convertToMap
} = require('./solution');
const Solutions = {
  countProducts,
  countProductChanges,
  countProductsGrandTotal,
  getMostViableGameChanger,
  convertToMap
};

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function queryDbFile(query) {
  return query(require('../db/data.json'));
}

function getMarketPayload(id) {
  return queryDbFile(db => db['global_markets'][id])
}

function getRandomMarketPayload() {
  const knownIds = [
    'cac0afc8-dc05-4870-91f2-c6b74fc25e6a',
    '627ac1ee-d225-486a-8a3c-b027aee16838',
    '0851a004-5fbc-48e6-b308-210c040173c5',
    'c05e4fe4-c3f7-49ca-8b92-b46bf5091bc7',
    '8aed93bc-fd46-4aef-98bd-a9eb83db83d6',
    '5b184638-36e1-4a00-9705-a63f092dddbf',
    '56f6e9d0-4f0c-41b2-87fd-06c853199cc6',
    '5924c815-eb8d-490a-ad22-df05ce8c952b',
    '4557f69e-0af3-4ae4-8a30-e900e33fa0bf',
    '42b6d973-000d-4dbd-9e10-7d6bbe6475c3'
  ];

  const idIndex = getRandomInt(0, knownIds.length - 1);
  const id = knownIds[idIndex];
  return getMarketPayload(id);
}

app.get('/mock-data', (req, res) => {
  try {
    res.json(
      getRandomMarketPayload()
    );
  } catch(err) {
    console.error(err);
    res.status(500).json({ message: 'Service error' });
  }
});

app.get('/mock-data/users', (req, res) => {
  try {
    const data = queryDbFile(db => db['users']);
    res.json(data);
  } catch(err) {
    console.error(err);
    res.status(500).json({ message: 'Service error' });
  }
});

function getSolutionResult(fn, resourceId) {
  try {
    const trueResult = getMarketPayload(resourceId);
    const users = queryDbFile(db => db['users']);
    const timingLabel = `\t[${ fn }] for ${ resourceId }`;
    console.time(timingLabel);
    const handler = Solutions[fn];
    const result = handler([trueResult], users);
    console.timeEnd(timingLabel);
    return Promise.resolve(result);
  } catch(err) {
    return Promise.reject(err);
  }
}

function handleGetSolutionResult(solutionId) {
  return (req, res) => {
    const resourceId = req.params['id'];
    return getSolutionResult(solutionId, resourceId).then(result => {
      res.json({
        resourceId,
        solutionId,
        result
      });
    }).catch(error => {
      error.details = {
        path: req.url,
        target: solutionId,
        resourceId
      }
      console.error(error);
      res.status(500).json({ error });
    });
  }
}

app.get('/countProducts/:id', handleGetSolutionResult('countProducts'));

app.get('/countProductChanges/:id', handleGetSolutionResult('countProductChanges'));

app.get('/countProductsGrandTotal/:id', handleGetSolutionResult('countProductsGrandTotal'));

app.get('/getMostViableGameChanger/:id', handleGetSolutionResult('getMostViableGameChanger'));

app.get('/convertToMap/:id', handleGetSolutionResult('convertToMap'));

module.exports.app = app;

if (require.main === module) {
  app.listen(TESTING_SERVER_PORT, () => {
    console.log('Testing server started on', `http://localhost:${ TESTING_SERVER_PORT }`);
  });
} else {
  console.log('Using as a plugin');
}

