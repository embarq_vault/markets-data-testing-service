// @ts-check

/**
 * @task
 * Count all the products of each of market
 * @expected
 * Products count(Number)
 */
const countProducts = (data) => {
  let res = 0;
  for (let section of data) {
    for (let market of section.markets) {
      for (let store of market.stores) {
        res += store.products.length;
      }
    }
  }
  return res;
}

/**
 * @task
 * Count all the products changes(`product.history` property entries)
 * @expected
 * Products changes count(Number)
 */
const countProductChanges = (data) => {
  let res = 0;

  for (let section of data) {
    for (let market of section.markets) {
      for (let store of market.stores) {
        for (let product of store.products) {
          res += product.history.length;
        }
      }
    }
  }

  return res;
}

/**
 * @task
 * For each product, count its grand-total following the formula:
 *    G = `product.cap + (product.history.change[0] + product.history.change[1] + ... + product.history.change[n])`
 * Write each result to the new property - `product.grandTotal`
 * If `product.history.issuer` is missing - the history entry should not be included in calculation
 * @expected
 * Mapped data object. Initial top-level data structure should not change
 */
const countProductsGrandTotal = (data) => {
  const evalProductCap = (cap) => {
    const lnsMapping = { M: 6, G: 9, T: 12, B: 12, P: 15, E: 18, Z: 21, Y: 24 };
    const [, amount, lns] = cap.match(/(\d+(?:\.\d+)?)([A-Z])/) || [null, null, null];
    return Number(amount) * Math.pow(10, lnsMapping[lns]);
  }

  let formatToLns = value => {
    const lnsMapping = { 6: 'M', 9: 'G', 12: 'T', 15: 'P', 18: 'E', 21: 'Z', 24: 'Y' };
    const [fixedAmount, fraction] = value.toString().split('.');
    // Large Number Symbol
    let lns = '';
    let largeNumberDiff = 0;

    for (let step in lnsMapping) {
      if ((fixedAmount.length - Number(step)) <= 0) {
        break;
      }

      largeNumberDiff = Number(step);
      lns = lnsMapping[step];
    }

    const lnsValue = fixedAmount.slice(0, fixedAmount.length - largeNumberDiff);
    const lnsFraction = fraction == null ? '' : '.' + fraction;
    return lnsValue + lnsFraction + lns;
  }

  for (let section of data) {
    for (let market of section.markets) {
      for (let store of market.stores) {
        for (let product of store.products) {
          const cap = (product.cap === 'n/a' || product.cap == null) ? 0 : evalProductCap(product.cap);
          const changeOverhead = product.history.reduce((sum, entry) => sum + entry.change, 0);
          product.grandTotal = formatToLns(cap + changeOverhead);
        }
      }
    }
  }
  return data;
}

/**
 * @task
 * Find a product change issuer which impacted the most products
 * @expected
 * Object of type `{ issuer: String, impact: Number }` where:
 *    - `issuer`: issuer data taken from `bank_users.json` by `issuerId`
 *    - `impact`: impacted products count
 */
const getMostViableGameChanger = (data, issuersData) => {
  let res = {};

  for (let section of data) {
    for (let market of section.markets) {
      for (let store of market.stores) {
        for (let product of store.products) {
          for (let entry of product.history) {
            if (entry.issuer == null) {
              continue;
            }

            res[entry.issuer] = Number.isFinite(res[entry.issuer]) ? (res[entry.issuer] + 1) : 1;
          }
        }
      }
    }
  }

  res = Object.entries(res).map(([key, value]) => `${ value }~${ key }`);
  res.sort();
  const [impact, issuerId] = res[res.length - 1].split('~');
  const issuer = issuersData.find(user => user.id === issuerId);

  return { issuer, impact: Number(impact) };
}

/**
 * @task
 * Remap data
 * - Convert existing data structure into dictionary-like(object) data structure
 * - Replace each product.history.issuer field value with corresponding issuer data from `bank_users.json`
 * @expected
 * Example of expected output https://imgur.com/LrztKcs
 */
const convertToMap = (data, issuersData) => {
  const result = data.reduce((globalMarkets, globalMarket) => {
    globalMarkets[globalMarket.id] = globalMarket.markets.reduce(function reduceMarkets(markets, market) {
      markets[market.key] = {
        title: market.key,
        stores: market.stores.reduce(function reduceStores(marketStores, marketStore) {
          marketStores[marketStore.key] = {
            title: marketStore.title,
            products: marketStore.products.reduce(function reduceMarketProducts(marketProducts, product) {
              marketProducts[product.key] = {
                ...product,
                history: product.history.reduce(function reduceProductHistory(productHistory, productHistoryEntry) {
                  productHistory[productHistoryEntry.timestamp] = {
                    change: productHistoryEntry.change,
                    issuer: issuersData.find(item => item.id === productHistoryEntry.issuer) || null
                  }
                  return productHistory;
                }, {})
              }
              return marketProducts;
            }, {})
          }
          return marketStores;
        }, {})
      }

      return markets;
    }, {});

    return globalMarkets;
  }, {});

  return result;
}

/* console.log(
  'countProducts',
  countProducts(MARKETS_DATA)
);
console.log(
  'countProductChanges',
  countProductChanges(MARKETS_DATA)
);
console.log(
  'countProductsGrandTotal',
  countProductsGrandTotal(MARKETS_DATA)
);
console.log(
  'getMostViableGameChanger',
  getMostViableGameChanger(MARKETS_DATA)
);
 */

module.exports = {
  countProducts,
  countProductChanges,
  countProductsGrandTotal,
  getMostViableGameChanger,
  convertToMap
}
